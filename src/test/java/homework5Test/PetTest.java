package homework5Test;

import java.util.Objects;
import org.homework5.Pet;
import org.homework5.Species;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

public class PetTest {
    private final PrintStream originalOut = System.out;

    @Test
    public void testEat() {
        Pet pet = new Pet();

        // Redirect the standard output stream
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call the eat() method
        pet.eat();

        // Reset the standard output stream
        System.setOut(originalOut);

        // Check only the output related to the eat() method
        String expectedOutput = "I'm eating!";
        String actualOutput = outContent.toString().trim();  // Remove leading/trailing whitespaces

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testRespond() {
        Pet pet = new Pet();
        pet.setNickname("TestPet");

        // Redirect the standard output stream
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        // Call the respond() method
        pet.respond();

        // Reset the standard output stream
        System.setOut(originalOut);

        // Check only the output related to the respond() method
        String expectedOutput = "Hello, owner. I'm TestPet. I'm bored!";
        String actualOutput = outContent.toString().trim();  // Remove leading/trailing whitespaces

        assertEquals(expectedOutput, actualOutput);
    }

    @Test
    public void testFoul() {
        // Redirect the standard output stream
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        Pet pet = new Pet();
        pet.foul();

        // Reset the standard output stream
        System.setOut(originalOut);

        // Check only the output related to the foul() method
        String expectedOutput = "I need to cover my tracks well...";
        String actualOutput = outContent.toString().trim();  // Remove leading/trailing whitespaces

        assertTrue(actualOutput.contains(expectedOutput));
    }

    @Test
    public void testEquals_and_hashCode() {
        Pet pet1 = new Pet(3, Species.PERSIAN_CAT, "Whiskers", 50, new String[]{"play", "sleep"});
        Pet pet2 = new Pet(3, Species.PERSIAN_CAT, "Whiskers", 50, new String[]{"play", "sleep"});
        Pet pet3 = new Pet(2, Species.LABRADOR, "Buddy", 60, new String[]{"fetch", "rollover"});

        // Testing reflexivity
        assertEquals(pet1, pet1);

        // Testing consistency
        assertEquals(pet1, pet2);

        // Testing symmetry
        assertEquals(pet2, pet1);

        // Testing transitivity
        assertEquals(pet1, pet2);
        assertEquals(pet2, pet1);
        assertEquals(pet1, pet2);

        // Testing equality with null
        assertNotEquals(pet1, null);

        // Testing hash code consistency
        assertEquals(pet1.hashCode(), pet2.hashCode());

        // Testing hash code inequality with different objects
        assertNotEquals(pet1.hashCode(), pet3.hashCode());

        // Override hashCode for pet1 to introduce a discrepancy
        pet1 = new Pet(3, Species.PERSIAN_CAT, "Whiskers", 50, new String[]{"play", "sleep"}) {
            @Override
            public int hashCode() {
                return Objects.hash(getAge());
            }
        };
        assertEquals(pet1, pet2);
        assertNotEquals(pet1.hashCode(), pet2.hashCode());

        // Changing a field after object creation
        pet1.setAge(4);
        assertNotEquals(pet1, pet2);  // pet1.equals(pet2) might return false
    }

    @Test
    public void toStringShouldReturnFormattedString() {
        String[] habits = {"Sleeping", "Playing"};
        Pet pet = new Pet(3, Species.LABRADOR, "Buddy", 80, habits);

        String expected = "LABRADOR{nickname='Buddy', age=3, trickLevel=80, habits=[Sleeping, Playing], canFly=false, hasFur=true, numberOfLegs=4}";
        String actual = pet.toString();

        assertEquals(expected, actual);
    }
}
