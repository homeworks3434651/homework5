package homework5Test;

import org.homework5.Family;
import org.homework5.*;

import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class HumanTest_hamcrest {

    @Test
    public void testSetFamily() {
        Family family = TestUtilities.setUpFamily();
        Human human3 = new Human("Alex", "Doe", 2000);

        assertThat(human3.setFamily(family), is(true));
        assertThat(human3.getFamily(), is(equalTo(family)));

        Family family2 = new Family(new Human("Bob", "Smith", 1990), new Human("Alice", "Smith", 1995));

        assertThat(human3.setFamily(family2), is(false));
        assertThat(human3.getFamily(), is(equalTo(family))); // Family should remain unchanged
    }

    @Test
    public void testFeedPetWhenTimeToFeed() {
        Family family = TestUtilities.setUpFamily();
        Pet pet = new Pet(Species.LABRADOR, "Buddy");
        pet.setTrickLevel(52);

        family.setPet(pet);

        // Test feeding when it's time to feed
        assertThat(family.getFather().feedPet(true), is(true));
    }

    @Test
    public void testFeedPet_shouldReturnTrueOrFalse() {
        Family family = TestUtilities.setUpFamily();
        Pet pet = new Pet(Species.PERSIAN_CAT, "Whiskers");
        // Assuming setTrickLevel generates a random value between 0 and 100
        pet.setTrickLevel(50);

        family.setPet(pet);

        // Now, instead of expecting a specific result, you can assert that it's either true or false
        assertThat(family.getFather().feedPet(false), anyOf(is(true), is(false)));
    }

    @Test
    public void testFeedPet() {
        Family family = TestUtilities.setUpFamily();
        Pet pet = new Pet(Species.PERSIAN_CAT, "Whiskers");
        family.setPet(pet);

        // Test when it's not time to feed, and pet trick level is higher
        family.getPet().setTrickLevel(100);
        assertThat(family.getFather().feedPet(false), is(true));

        // Test when it's not time to feed, and pet trick level is small
        family.getPet().setTrickLevel(0);
        assertThat(family.getFather().feedPet(false), is(false));
    }

    @Test
    public void testGreetPet() {
        Family family = TestUtilities.setUpFamily();
        Pet pet = new Pet(Species.PERSIAN_CAT, "Whiskers");
        family.setPet(pet);

        // Test greeting when there is a pet
        assertThat(family.getMother().greetPet(), is(equalTo("Hello, Whiskers")));

        // Test greeting when there is no pet
        family.setPet(null);
        assertThat(family.getMother().greetPet(), is(equalTo("I don't have a pet.")));
    }

    @Test
    public void testDescribePet() {
        Family family = TestUtilities.setUpFamily();
        Pet pet = new Pet(Species.LABRADOR, "Buddy");
        pet.setAge(3);
        pet.setTrickLevel(75);
        family.setPet(pet);

        // Test describing the pet
        assertThat(family.getMother().describePet(), is(equalTo("I have a LABRADOR. It is 3 years old, and it is very cunning.")));

        // Test describing when there is no pet
        family.setPet(null);
        assertThat(family.getMother().describePet(), is(equalTo("I don't have a pet.")));
    }

    @Test
    public void testEquals_and_hashCode() {
        Human human1 = new Human("John", "Doe", 1980);
        Human human2 = new Human("John", "Doe", 1980);
        Human human3 = new Human("Jane", "Doe", 1985);

        assertThat(human1, is(equalTo(human2)));
        assertThat(human1, is(not(equalTo(human3))));

        assertThat(human1.hashCode(), is(equalTo(human2.hashCode())));
        assertThat(human1.hashCode(), is(not(equalTo(human3.hashCode()))));
    }

    @Test
    public void testToString() {
        Human human = new Human("John", "Doe", 1980);
        human.setIQ(120);

        // Test the string representation of the human
        assertThat(human.toString(), is(equalTo("Human{name='John', surname='Doe', year=1980, iq=120, schedule=null}")));
    }
}
