package homework5Test;

import org.homework5.Family;
import org.homework5.Human;

public class TestUtilities {

    public static Family setUpFamily() {
        Human father = new Human("John", "Doe", 1970);
        Human mother = new Human("Jane", "Doe", 1975);
        Family family = new Family(father, mother);
        return family;
    }
}
