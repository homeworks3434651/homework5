package homework5Test;

import org.homework5.Family;
import org.homework5.Human;
import org.homework5.Pet;
import org.homework5.Species;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Objects;

import static org.assertj.core.api.Assertions.*;

public class FamilyTest_assertj {

    @Test
    public void addChildShouldAddChildToFamily() {
        Family family = TestUtilities.setUpFamily();
        Human child = new Human("Bob", "Doe", 1995);

        assertThat(family.addChild(child)).isTrue();
        assertThat(Arrays.asList(family.getChildren())).contains(child);
        assertThat(family.getChildren()).hasSize(1);
        assertThat(family.getChildren()[0]).isSameAs(child);
        assertThat(child.getFamily()).isSameAs(family);
    }

    @Test
    public void addChildShouldNotAddDuplicateChild() {
        Family family = TestUtilities.setUpFamily();
        Human child = new Human("Alex", "Doe", 2000);

        assertThat(family.addChild(child)).isTrue();
        assertThat(family.addChild(child)).isFalse();
    }

    @Test
    public void deleteChildShouldRemoveChildFromFamily() {
        Family family = TestUtilities.setUpFamily();
        Human child1 = new Human("Bob", "Doe", 1995);
        Human child2 = new Human("Alex", "Doe", 2000);

        assertThat(family.addChild(child1)).isTrue();
        assertThat(family.addChild(child2)).isTrue();

        family.deleteChild(child1);

        assertThat(Arrays.asList(family.getChildren())).doesNotContain(child1);

        Human nonExistentChild = new Human("Non", "Existent", 1998);
        int originalChildrenCount = family.getChildren().length;

        family.deleteChild(nonExistentChild);

        assertThat(family.getChildren()).hasSize(originalChildrenCount);
    }

    @Test
    public void deleteChildByIndexShouldRemoveChildFromFamily() {
        Family family = TestUtilities.setUpFamily();
        Human child1 = new Human("Bob", "Doe", 1995);
        Human child2 = new Human("Alex", "Doe", 2000);

        assertThat(family.addChild(child1)).isTrue();
        assertThat(family.addChild(child2)).isTrue();

        assertThat(family.deleteChild(0)).isTrue();
        assertThat(Arrays.asList(family.getChildren())).doesNotContain(child1);

        assertThat(family.deleteChild(2)).isFalse();
        assertThat(family.getChildren()).hasSize(1);
    }

    @Test
    public void testCountFamily() {
        Family family = TestUtilities.setUpFamily();

        assertThat(family.countFamily()).isEqualTo(2);

        Human child1 = new Human("Bob", "Doe", 1995);
        Human child2 = new Human("Alex", "Doe", 2000);

        assertThat(family.addChild(child1)).isTrue();
        assertThat(family.addChild(child2)).isTrue();

        assertThat(family.countFamily()).isEqualTo(4);
        family.deleteChild(child1);
        assertThat(family.countFamily()).isEqualTo(3);
    }

    @Test
    public void testSetFather_and_Mother() {
        Family family = new Family(null, null);
        Human father = new Human("John", "Doe", 1970);
        family.setFather(father);

        assertThat(family.getFather()).isEqualTo(father);
        assertThat(father.getFamily()).isEqualTo(family);

        Human mother = new Human("Jane", "Doe", 1975);
        family.setMother(mother);

        Human newFather = new Human("Bob", "Kennedy", 1970);
        family.setFather(newFather);

        assertThat(newFather.getFamily()).isNull();

        Human newMother = new Human("Marina", "Krotova", 1980);
        family.setMother(newMother);

        assertThat(newMother.getFamily()).isNull();
    }

    @Test
    public void testEquals_and_hashCode() {
        Family family1 = TestUtilities.setUpFamily();
        Human father2 = new Human("John", "Doe", 1970);
        Human mother2 = new Human("Jane", "Doe", 1975);
        Family family2 = new Family(father2, mother2);
        Human father3 = new Human("Charlie", "Brown", 1990);
        Human mother3 = new Human("Lucy", "Brown", 1992);
        Family family3 = new Family(father3, mother3);

        // Testing reflexivity
        assertThat(family1).isEqualTo(family1);

        // Testing consistency
        assertThat(family1).isEqualTo(family2);

        // Testing symmetry
        assertThat(family2).isEqualTo(family1);

        // Testing transitivity
        assertThat(family1).isEqualTo(family2)
                .isEqualTo(family1)
                .isEqualTo(family2);

        // Testing equality with null
        assertThat(family1).isNotEqualTo(null);

        // Testing hash code consistency
        assertThat(family1.hashCode()).isEqualTo(family2.hashCode());

        // Testing hash code inequality with different objects
        assertThat(family1.hashCode()).isNotEqualTo(family3.hashCode());
        assertThat(family1).isEqualTo(family2);

        // Override hashCode for family1 to introduce a discrepancy
        family2 = new Family(father2, mother2) {
            @Override
            public int hashCode() {
                return Objects.hash(getFather(), getMother());
            }
        };

        assertThat(family1).isNotEqualTo(family2);
        assertThat(family1.hashCode()).isNotEqualTo(family2.hashCode());

        Human father4 = new Human("Charlie", "Brown", 1990);
        Human mother4 = new Human("Lucy", "Brown", 1992);
        Family family4 = new Family(father4, mother4);
        Human son_family3 = new Human("Bryan", "Brown", 2010);
        family4.addChild(son_family3);
        assertThat(family3).isNotEqualTo(family4);
    }

    @Test
    public void testSetChildren() {
        Family family = TestUtilities.setUpFamily();

        Human child1 = new Human("Bob", "Doe", 1995);
        Human child2 = new Human("Alex", "Doe", 2000);

        Human[] newChildren = {child1, child2};

        family.setChildren(newChildren);

        assertThat(Arrays.asList(family.getChildren())).contains(child1, child2);
        assertThat(child1.getFamily()).isEqualTo(family);
        assertThat(child2.getFamily()).isEqualTo(family);
    }

    @Test
    public void testToString() {
        Family family = TestUtilities.setUpFamily();
        Human child1 = new Human("Bob", "Doe", 2005);
        Human child2 = new Human("Alice", "Doe", 2010);
        family.addChild(child1);
        family.addChild(child2);

        Pet myPet = new Pet(Species.LABRADOR, "Buddy");
        family.setPet(myPet);

        String familyString = family.toString();

        String expectedOutput = "Family{\n" +
                "  Father: " + family.getFather() + "\n" +
                "  Mother: " + family.getMother() + "\n" +
                "  Children: [" + child1 + ", " + child2 + "]\n" +
                "  Pet: " + myPet + "\n" +
                "  Total Persons in Family: 4\n" +
                "}";
        assertThat(familyString).isEqualTo(expectedOutput);
    }

    @Test
    public void testToStringWithNoChildrenAndNoPet() {
        Family family = TestUtilities.setUpFamily();
        String familyString = family.toString();

        String expectedOutput = "Family{\n" +
                "  Father: " + family.getFather() + "\n" +
                "  Mother: " + family.getMother() + "\n" +
                "  Children: No children in this family.\n" +
                "  Pet: null\n" +
                "  Total Persons in Family: 2\n" +
                "}";
        assertThat(familyString).isEqualTo(expectedOutput);
    }
}
