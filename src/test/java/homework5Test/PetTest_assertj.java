package homework5Test;

import org.homework5.Pet;
import org.homework5.Species;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Objects;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class PetTest_assertj {

    private final PrintStream originalOut = System.out;

    @Test
    public void testEat() {
        Pet pet = new Pet();

        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        pet.eat();

        System.setOut(originalOut);

        assertThat(outContent.toString().trim()).isEqualTo("I'm eating!");
    }

    @Test
    public void testRespond() {
        Pet pet = new Pet();
        pet.setNickname("TestPet");

        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        pet.respond();

        System.setOut(originalOut);

        assertThat(outContent.toString().trim())
                .isEqualTo("Hello, owner. I'm TestPet. I'm bored!");
    }

    @Test
    public void testFoul() {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        Pet pet = new Pet();
        pet.foul();

        System.setOut(originalOut);

        assertThat(outContent.toString().trim())
                .contains("I need to cover my tracks well...");
    }

    @Test
    public void testEquals_and_hashCode() {
        Pet pet1 = new Pet(3, Species.PERSIAN_CAT, "Whiskers", 50, new String[]{"play", "sleep"});
        Pet pet2 = new Pet(3, Species.PERSIAN_CAT, "Whiskers", 50, new String[]{"play", "sleep"});
        Pet pet3 = new Pet(2, Species.LABRADOR, "Buddy", 60, new String[]{"fetch", "rollover"});

        // Testing reflexivity
        assertThat(pet1).isEqualTo(pet1);

        // Testing consistency
        assertThat(pet1).isEqualTo(pet2);

        // Testing symmetry
        assertThat(pet2).isEqualTo(pet1);

        // Testing transitivity
        assertThat(pet1).isEqualTo(pet2).isEqualTo(pet1);

        // Testing equality with null
        assertThat(pet1).isNotEqualTo(null);

        // Testing hash code consistency
        assertThat(pet1.hashCode()).isEqualTo(pet2.hashCode());

        // Testing hash code inequality with different objects
        assertThat(pet1.hashCode()).isNotEqualTo(pet3.hashCode());

        // Override hashCode for pet1 to introduce a discrepancy
        pet1 = new Pet(3, Species.PERSIAN_CAT, "Whiskers", 50, new String[]{"play", "sleep"}) {
            @Override
            public int hashCode() {
                return Objects.hash(getAge());
            }
        };

        assertThat(pet1.hashCode()).isNotEqualTo(pet2.hashCode());

        // Changing a field after object creation
        pet1.setAge(4);
        assertThat(pet1).isNotEqualTo(pet2);  // pet1.equals(pet2) might return false
    }

    @Test
    public void toStringShouldReturnFormattedString() {
        String[] habits = {"Sleeping", "Playing"};
        Pet pet = new Pet(3, Species.LABRADOR, "Buddy", 80, habits);

        String expected = "LABRADOR{nickname='Buddy', age=3, trickLevel=80, habits=[Sleeping, Playing], canFly=false, hasFur=true, numberOfLegs=4}";
        String actual = pet.toString();

        assertThat(actual).isEqualTo(expected);
    }
}
