package homework5Test;

import org.homework5.*;

import java.util.Arrays;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class HumanTest {

    @Test
    public void testSetFamily() {
        Family family = TestUtilities.setUpFamily();
        Human human3 = new Human("Alex", "Doe", 2000);

        assertTrue(human3.setFamily(family));
        assertEquals(family, human3.getFamily());

        Family family2 = new Family(new Human("Bob", "Smith", 1990), new Human("Alice", "Smith", 1995));

        assertFalse(human3.setFamily(family2));
        assertEquals(family, human3.getFamily()); // Family should remain unchanged
    }

    @Test
    public void testFeedPetWhenTimeToFeed() {
        Family family = TestUtilities.setUpFamily();
        Pet pet = new Pet(Species.LABRADOR, "Buddy");
        pet.setTrickLevel(52);

        family.setPet(pet);

        // Test feeding when it's time to feed
        assertTrue(family.getFather().feedPet(true));
    }

    @Test
    public void testFeedPet_shouldReturnTrueOrFalse() {
        Family family = TestUtilities.setUpFamily();
        Pet pet = new Pet(Species.PERSIAN_CAT, "Whiskers");
        // Assuming setTrickLevel generates a random value between 0 and 100
        pet.setTrickLevel(50);

        family.setPet(pet);

        // Now, instead of expecting a specific result, you can assert that it's either true or false
        assertTrue(Arrays.asList(true, false).contains(family.getFather().feedPet(false)));
    }

    @Test
    public void testFeedPet() {
        Family family = TestUtilities.setUpFamily();
        Pet pet = new Pet(Species.PERSIAN_CAT, "Whiskers");
        family.setPet(pet);

        // Test when it's not time to feed, and pet trick level is higher
        family.getPet().setTrickLevel(100);
        assertTrue(family.getFather().feedPet(false));

        // Test when it's not time to feed, and pet trick level is small
        family.getPet().setTrickLevel(0);
        assertFalse(family.getFather().feedPet(false));
    }

    @Test
    public void testGreetPet() {
        Family family = TestUtilities.setUpFamily();
        Pet pet = new Pet(Species.PERSIAN_CAT, "Whiskers");
        family.setPet(pet);

        // Test greeting when there is a pet
        String greetingWithPet = family.getMother().greetPet();
        System.out.println(greetingWithPet);
        assertEquals("Hello, Whiskers", greetingWithPet);

        // Test greeting when there is no pet
        family.setPet(null);
        String greetingWithoutPet = family.getMother().greetPet();
        System.out.println(greetingWithoutPet);
        assertEquals("I don't have a pet.", greetingWithoutPet);
    }

    @Test
    public void testDescribePet() {
        Family family = TestUtilities.setUpFamily();
        Pet pet = new Pet(Species.LABRADOR, "Buddy");
        pet.setAge(3);
        pet.setTrickLevel(75);
        family.setPet(pet);

        // Test describing the pet
        String describePet = family.getMother().describePet();
        System.out.println(describePet);
        assertEquals("I have a LABRADOR. It is 3 years old, and it is very cunning.", describePet);

        // Test describing when there is no pet
        family.setPet(null);
        String describeNoPet = family.getMother().describePet();
        System.out.println(describeNoPet);
        assertEquals("I don't have a pet.", describeNoPet);
    }

    @Test
    public void testEquals_and_hashCode() {
        Human human1 = new Human("John", "Doe", 1980);
        Human human2 = new Human("John", "Doe", 1980);
        Human human3 = new Human("Jane", "Doe", 1985);

        assertEquals(human1, human2);
        assertNotEquals(human1, human3);

        assertEquals(human1.hashCode(), human2.hashCode());
        assertNotEquals(human1.hashCode(), human3.hashCode());
    }

    @Test
    public void testToString() {
        Human human = new Human("John", "Doe", 1980);
        human.setIQ(120);

        // Test the string representation of the human
        assertEquals("Human{name='John', surname='Doe', year=1980, iq=120, schedule=null}", human.toString());
    }
}
