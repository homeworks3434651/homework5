package org.homework5;

public class MainGarbageCollector {

    public static void main(String[] args) {
        int objectsCount = 10000000; // Adjust the number of objects based on available memory

        for (int i = 0; i < objectsCount; i++) {
            new Human("Name" + i, "Surname" + i, 1990 + i);
        }

        System.out.println("Objects created. Waiting for garbage collection...");

        // Trigger garbage collection
        System.gc();

        // Allow some time for garbage collection to run
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
