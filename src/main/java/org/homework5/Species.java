package org.homework5;

public enum Species {
    GOLDEN_RETRIEVER(false, 4, true),
    LABRADOR(false, 4, true),
    PERSIAN_CAT(false, 4, true),
    PARROT(true, 2, false),
    FISH(false, 0, false),
    RABBIT(),
    HAMSTER(false, 4, true),
    SNAKE(false, 0, false),
    TURTLE(false, 4, false),
    HORSE(false, 4, true),
    GUINEA_PIG(false, 4, true);

    private boolean canFly;
    private int numberOfLegs;
    private boolean hasFur;

    Species(boolean canFly, int numberOfLegs, boolean hasFur) {
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }

    Species() {

    }

    public boolean canFly() {
        return canFly;
    }

    public void setCanFly(boolean canFly) {
        this.canFly = canFly;
    }

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public void setNumberOfLegs(int numberOfLegs) {
        this.numberOfLegs = numberOfLegs;
    }

    public boolean hasFur() {
        return hasFur;
    }

    public void setHasFur(boolean hasFur) {
        this.hasFur = hasFur;
    }
}
