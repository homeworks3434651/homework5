package org.homework5;

public enum DayOfWeek {
    MONDAY(""),
    TUESDAY(""),
    WEDNESDAY(""),
    THURSDAY(""),
    FRIDAY(""),
    SATURDAY(""),
    SUNDAY("");

    private String activity;

    DayOfWeek(String activity) {
        this.activity = activity;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public static String getActivityForDay(DayOfWeek day, String[][] schedule) {
        for (String[] dayActivity : schedule) {
            if (dayActivity[0].equals(day.name())) {
                return dayActivity[1];
            }
        }
        return "";  // Return an empty string if no activity is found
    }
}
