package org.homework5;

import java.util.Arrays;
import java.util.Objects;

public class Pet {
    private int age;
    private Species species;
    private String nickname;
    private int trickLevel;
    private String[] habits;

    static {
        System.out.println("Pet class is loaded.");
    }

    {
        System.out.println("A new Pet object is created.");
    }

    public Pet() {
        // Empty constructor
    }

    public Pet(Species species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public Pet(int age, Species species, String nickname, int trickLevel, String[] habits) {
        this.age = age;
        this.species = species;
        this.nickname = nickname;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public void eat() {
        System.out.println("I'm eating!");
    }

    public void respond() {
        System.out.println("Hello, owner. I'm " + getNickname() + ". I'm bored!");
    }

    public void foul() {
        System.out.println("I need to cover my tracks well...");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pet)) return false;

        Pet pet = (Pet) o;

        return getAge() == pet.getAge() &&
               getSpecies() == pet.getSpecies() &&
               Objects.equals(getNickname(), pet.getNickname());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAge(), getSpecies(), getNickname());
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            System.out.println("Finalizing Pet object: " + this);
        } finally {
            super.finalize();
        }
    }

    @Override
    public String toString() {
        return species + "{" +
                "nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                ", canFly=" + species.canFly() +
                ", hasFur=" + species.hasFur() +
                ", numberOfLegs=" + species.getNumberOfLegs() +
                '}';
    }
}
